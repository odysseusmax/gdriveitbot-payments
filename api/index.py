import os

from flask import Flask, redirect

app = Flask(__name__)

PAYMENT_MODE_PAYPAL = "PAYPAL"
PAYMENT_MODE_UPI = "UPI"
ALLOWED_PAYMENT_MODES = (PAYMENT_MODE_PAYPAL, PAYMENT_MODE_UPI)
PLAN_1 = 1
PLAN_2 = 2
PLAN_3 = 3
ALLOWED_PLANS = (PLAN_1, PLAN_2, PLAN_3)
REDIRECT_URLS = {
    PAYMENT_MODE_PAYPAL: {
        PLAN_1: os.getenv("PAYPAL_PLAN1_URL", ""),
        PLAN_2: os.getenv("PAYPAL_PLAN2_URL", ""),
        PLAN_3: os.getenv("PAYPAL_PLAN3_URL", ""),
    },
    PAYMENT_MODE_UPI: {
        PLAN_1: os.getenv(
            "UPI_PLAN1_URL",
            "",
        ),
        PLAN_2: os.getenv(
            "UPI_PLAN2_URL",
            "",
        ),
        PLAN_3: os.getenv(
            "UPI_PLAN3_URL",
            "",
        ),
    },
}
REDIRECT_URL = os.getenv("REDIRECT_URL", "")


@app.route("/mode/<payment_mode>/plan<int:plan_no>")
def payment_mode(payment_mode: str, plan_no: int):
    payment_mode = payment_mode.upper()
    if payment_mode not in ALLOWED_PAYMENT_MODES:
        return redirect("/")

    if plan_no not in ALLOWED_PLANS:
        return redirect("/")

    redirect_url = REDIRECT_URLS[payment_mode][plan_no]
    return redirect(redirect_url)


@app.route("/", defaults={"path": ""})
@app.route("/<path:path>")
def home(path):
    return redirect(REDIRECT_URL)


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
